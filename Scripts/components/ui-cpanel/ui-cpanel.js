(function () {
    // Localize jQuery variable
    var jQuery;

    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '3.1.1') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src",
            "http://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js");
        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () { // For old versions of IE
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        } else { // Other browsers
            script_tag.onload = scriptLoadHandler;
        }
        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

    /******** Called once jQuery has loaded ******/
    function scriptLoadHandler() {
        // Restore $ and window.jQuery to their previous values and store the
        // new jQuery in our local jQuery variable
        jQuery = window.jQuery.noConflict(true);
        // Call our main function
        main();
    }

    /******** Our main function ********/
    function main() {
        var uiCpanel;
        jQuery(document).ready(function ($) {

            /******* Load CSS *******/
            var css_style = uiCpanel_cssStyles();
            jQuery("head").append(css_style);

            /******* Load HTML *******/
            uiCpanel = '<div id="ui-cpanel-widget">';
                uiCpanel += '<div class="ui-cpanel-toggle"><i class="material-icons">settings</i></div>';
                uiCpanel += '<div class="ui-cpanel-title">UI Control Panel <i class="material-icons ui-cpanel-close">clear</i></div>';

                uiCpanel += uiCpanel_userStates();
                uiCpanel += uiCpanel_toggleErrors();
                uiCpanel += uiCpanel_toggleSections();

            uiCpanel += '</div>';

            uiCpanel += '<div id="ui-cpanel-overlay"></div>';
            jQuery("body").append(uiCpanel);

            /******* Events *******/
            jQuery(document).on('click', '#ui-cpanel-widget .ui-cpanel-toggle, #ui-cpanel-widget .ui-cpanel-title .ui-cpanel-close', function () {
                jQuery("#ui-cpanel-widget").toggleClass("open");
            });

            jQuery(document).on('click', '.form-group .ui-cpanel-info-button', function () {
                jQuery(this).toggleClass("active").parent().children(".ui-cpanel-info").toggleClass("open");
            });

            jQuery(document).on('click', '.form-group .ui-cpanel-overview', function () {
                if (jQuery('.ui-cpanel-overview.active').length == 0) {
                    jQuery("body").removeClass("overlay-active");
                } else {
                    jQuery("body").addClass("overlay-active");
                }
            });

            /******* Check for existing classes *******/
            if (jQuery(".logged, .guest").length == 0) {
                jQuery("#ui-cpanel-user").addClass("disabled");
            }
            if (jQuery(".error").length == 0) {
                jQuery("#ui-cpanel-errors").addClass("disabled");
            }
            if (jQuery('[class^="ui-cpanel-section-"], [class*=" ui-cpanel-section-"]').length == 0) {
                jQuery("#ui-cpanel-sections").addClass("disabled");
            }
        });
    }

    function uiCpanel_userStates() {
        /******* Load HTML *******/
        var s;
        s = '<div class="form-group" id="ui-cpanel-user">';
        s += '<i class="material-icons pull-right ui-cpanel-info-button" title="See more info">help</i>';
        s += '<i class="material-icons pull-right ui-cpanel-overview" title="Highlight">desktop_mac</i>';
        s += ' <label>User state :</label> <select name="ui-cpanel-user" class="form-control">';
            s += '<option value="0">Show both states</option>';
            s += '<option value="1" selected>Guest</option>';
            s += '<option value="2">Logged</option>';
            s += '</select>';
            s += '<div class="ui-cpanel-info">This action shows additional sections marked with these classes : <br/> <ul><li><span>.logged</span> - will show sections for logged in users</li><li><span>.guest</span> - will show sections for guest/not logged in users</li></ul></div>';
        s += '</div>';

        /******* Events *******/
        jQuery(document).on('change', '[name="ui-cpanel-user"]', function () {
            switch (jQuery(this).val()) {
                case '1':
                    jQuery('.logged').hide();
                    jQuery('.guest').show();
                    break;
                case '2':
                    jQuery('.logged').show();
                    jQuery('.guest').hide();
                    break;
                default:
                    jQuery('.logged').show();
                    jQuery('.guest').show();
            }
        });

        //******* Overlay classes *******/
        jQuery(document).on('click', '#ui-cpanel-user .ui-cpanel-overview', function () {
            jQuery('[name="ui-cpanel-user"]').change();
            jQuery(this).toggleClass("active");

            if (jQuery(this).hasClass('active')) {
                jQuery('.logged, .guest').addClass('ui-cpanel-overlay-element');
            } else {
                jQuery('.logged, .guest').removeClass('ui-cpanel-overlay-element');
            }
        });

        return s;
    }

    function uiCpanel_toggleErrors() {
        /******* Load HTML *******/
        var s;
        s = '<div class="form-group" id="ui-cpanel-errors">';
        s += '<i class="material-icons pull-right ui-cpanel-info-button" title="See more info">help</i>';
        s += '<i class="material-icons pull-right ui-cpanel-overview" title="Highlight">desktop_mac</i>';
        s += '<label class="checkbox"><input type="checkbox" name="ui-cpanel-errors" /> Show errors</label>';
            s += '<div class="ui-cpanel-info">This action shows/hides the form error sections marked with class <span>.error</span></div>';
        s += '</div>';

        /******* Events *******/
        jQuery(document).on('click', '[name="ui-cpanel-errors"]', function () {
            if (jQuery(this).is(':checked')) {
                $('.error').show().removeClass('hidden');
            } else {
                $('.error').hide().addClass('hidden');
            }
        });

        //******* Overlay classes *******/
        jQuery(document).on('click', '#ui-cpanel-errors .ui-cpanel-overview', function () {
            jQuery(this).toggleClass("active");

            if (jQuery(this).hasClass('active')) {
                jQuery('.error').addClass('ui-cpanel-overlay-element');
            } else {
                jQuery('.error').removeClass('ui-cpanel-overlay-element');
            }
        });

        return s;
    }

    function uiCpanel_toggleSections() {
        /******* Load HTML *******/
        var s;
        s = '<div class="form-group" id="ui-cpanel-sections">';
        s += '<i class="material-icons pull-right ui-cpanel-info-button" title="See more info">help</i>';
        s += '<i class="material-icons pull-right ui-cpanel-overview" title="Highlight">desktop_mac</i>';
        s += '<label>Toggle sections :</label> <select name="ui-cpanel-sections" class="form-control">';
        s += '<option value="0">Show all sections</option>';
        s += '<option value="1" selected>Section 1</option>';
        s += '<option value="2">Section 2</option>';
        s += '</select>';
        s += '<div class="ui-cpanel-info">This action shows different sections marked with class : <span>.ui-cpanel-section-#</span></div>';
        s += '</div>';

        /******* Events *******/
        jQuery(document).on('change', '[name="ui-cpanel-sections"]', function () {
            if (jQuery(this).val() == 0) {
                jQuery('[class^="ui-cpanel-section-"], [class*=" ui-cpanel-section-"]').show().removeClass('hidden');
            } else {
                jQuery('[class^="ui-cpanel-section-"], [class*=" ui-cpanel-section-"]').hide().addClass('hidden')
                jQuery('.ui-cpanel-section-' + jQuery(this).val()).show().removeClass('hidden');
            }
        });

        //******* Overlay classes *******/
        jQuery(document).on('click', '#ui-cpanel-sections .ui-cpanel-overview', function () {
            jQuery('[name="ui-cpanel-sections"]').change();
            jQuery(this).toggleClass("active");

            if (jQuery(this).hasClass('active')) {
                jQuery('[class^="ui-cpanel-section-"], [class*=" ui-cpanel-section-"]').addClass('ui-cpanel-overlay-element');
            } else {
                jQuery('[class^="ui-cpanel-section-"], [class*=" ui-cpanel-section-"]').removeClass('ui-cpanel-overlay-element');
            }
        });

        return s;
    }

    function uiCpanel_cssStyles() {
        var mainColor = '#333';
        var actionColor = '#AAA';
        var highlightColor = '#53317b';
        var s;
        s = '<style>';

        s += '@import url("https://fonts.googleapis.com/icon?family=Material+Icons");';

        // Widget container
        s += '#ui-cpanel-widget, #ui-cpanel-widget * {';
        s += 'box-sizing: border-box';
        s += '}';

        s += '#ui-cpanel-widget {';
        s += 'width:250px; color:' + mainColor + '; position:fixed; left:0; top:0; height:100%; background:#fff; border:1px solid #ccc; z-index:9999; padding:15px; box-shadow: 0 1px 3px rgba(0,0,0,0.15); transition:0.3s; transform: translateX(-100%);';
        s += '}';

        s += '#ui-cpanel-widget.open {';
        s += 'transform: translateX(0);';
        s += '}';

        // Widget title
        s += '#ui-cpanel-widget .ui-cpanel-title {';
        s += 'border-bottom: 1px solid #ddd; background:#f0f0f0; font-size:15px; padding:10px 15px; margin:-15px -15px 0px; font-weight:700;';
        s += '}';

        s += '#ui-cpanel-widget .ui-cpanel-title .material-icons {';
        s += 'float:right; font-size:21px; color:' + actionColor + '; cursor:pointer;';
        s += '}';

        // Label
        s += '#ui-cpanel-widget label {';
        s += 'font-size:12px; display:inline-block; padding-right:5px';
        s += '}';


        // Widget toggle
        s += '#ui-cpanel-widget .ui-cpanel-toggle {';
        s += 'cursor:pointer; position:absolute; right:0;  top:60px; padding:5px; background:#fff; border-radius: 0 4px 4px 0; box-shadow: 5px 0 3px -2px rgba(0,0,0,0.15) inset, 0 1px 3px rgba(0,0,0,0.15); display:block; transform: translateX(100%)';
        s += '}';

        s += '#ui-cpanel-widget.open .ui-cpanel-toggle {';
        s += 'display:none';
        s += '}';

        s += '#ui-cpanel-widget .ui-cpanel-toggle .material-icons {';
        s += 'font-size:30px; color:' + highlightColor + '; animation: cog 5s infinite; animation-timing-function: linear; float:left;';
        s += '}';

        s += '@-webkit-keyframes cog { 100%{ -webkit-transform: rotate(360deg)} }';
        s += '@-moz-keyframes cog { 100%{ -webkit-transform: rotate(360deg)} }';
        s += '@-ms-keyframes cog {  100%{ -webkit-transform: rotate(360deg)} }';
        s += '@keyframes cog { 100%{ -webkit-transform: rotate(360deg); -moz-transform: rotate(360deg); -ms-transform: rotate(360deg); transform: rotate(360deg)}}';

        // Widget form controls,groups
        s += '#ui-cpanel-widget .form-control {';
        s += 'display: block; width: 100%; height: 34px; padding: 6px 12px; font-size: 13px; line-height: 1.428571429; color: ' + mainColor + '; vertical-align: middle; background-color: #fff; background-image: none; border: 1px solid #ccc; border-radius: 4px; -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075); box-shadow: inset 0 1px 1px rgba(0,0,0,0.075); -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s; transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;';
        s += '}';

        s += '#ui-cpanel-widget label {';
        s += 'font-weight:700; padding:0 0 10px 0; display:block;';
        s += '}';

        s += '#ui-cpanel-widget .form-group .material-icons{';
        s += 'font-size:17px; color:' + actionColor + '; cursor:pointer;';
        s += '}';

        s += '#ui-cpanel-widget .form-group .material-icons + .material-icons{';
        s += 'margin-right:5px;';
        s += '}';

        s += '#ui-cpanel-widget .form-group .material-icons:hover, #ui-cpanel-widget .form-group .material-icons.active {';
        s += 'color:' + highlightColor + ';';
        s += '}';

        s += '#ui-cpanel-widget .form-group {';
        s += 'margin:0 -15px; padding:15px; border-bottom:1px solid #ddd;';
        s += '}';

        s += '#ui-cpanel-widget .form-group.disabled {';
        s += 'position:relative;';
        s += '}';

        s += '#ui-cpanel-widget .form-group.disabled:before {';
        s += 'content:""; position:absolute; left:0; right:0; top:0; bottom:0; width:100%; height:100%; cursor:not-allowed; background: rgba(255,255,255,0.7)';
        s += '}';

        s += '#ui-cpanel-widget .form-group .checkbox{';
        s += 'line-height:13px; padding:0; cursor:pointer;';
        s += '}';

        s += '#ui-cpanel-widget .form-group .checkbox input[type="checkbox"]{';
        s += 'float:left; margin:0 13px 0 0; ';
        s += '}';


        // Widget infos
        s += '#ui-cpanel-widget .ui-cpanel-info {';
        s += 'font-size:11px; display:none; font-style:italic; color:' + actionColor + '; padding:15px 0 0 0;';
        s += '}';

        s += '#ui-cpanel-widget .ui-cpanel-info.open {';
        s += 'display:block;';
        s += '}';

        s += '#ui-cpanel-widget .ui-cpanel-info span {';
        s += 'font-weight:bold; color:' + mainColor + '; ';
        s += '}';

        s += '#ui-cpanel-widget .ui-cpanel-info ul {';
        s += 'padding-left:15px; ';
        s += '}';

        // Widget general
        s += '#ui-cpanel-widget .pull-right {';
        s += 'float:right;';
        s += '}';


        // Widget overlay
        /*s += '#ui-cpanel-overlay {';
        s += 'display:none; width: 100%; height: 100%; background: rgba(0,0,0,0.25); position: fixed; z-index: 8999; left: 0; top: 0;';
        s += '}';
        
        s += '.overlay-active #ui-cpanel-overlay {';
        s += 'display:block;';
        s += '}';*/

        s += '.overlay-active .ui-cpanel-overlay-element {';
        s += 'position:relative; z-index:99;';
        s += '}';

        s += '.overlay-active .ui-cpanel-overlay-element:after {';
        s += 'left:0; top:0; content:""; width: calc(100% + 30px); height: calc(100% + 30px); position:absolute; margin:-15px; box-shadow: 0 0 15px 0 #999; display:block; z-index:-1; background: rgba(255,255,255,0.5);';
        s += '}';

        /*s += '.overlay-active .ui-cpanel-overlay-element +  .ui-cpanel-overlay-element{';
        s += 'margin-top:30px;';
        s += '}';*/

        s += '</style>';
        return s;
    }
})(); // We call our anonymous function immediately